package find

def findAllAndPrint(entry: cs214.Entry): Boolean =
  println("/some/path1")
  println("/some/path2")
  true

def findByNameAndPrint(entry: cs214.Entry, name: String): Boolean =
  println("/some/path1")
  println("/some/path2")
  true

def findBySizeEqAndPrint(entry: cs214.Entry, size: Long): Boolean =
  println("/some/path1")
  println("/some/path2")
  true

def findBySizeGeAndPrint(entry: cs214.Entry, minSize: Long): Boolean =
  println("/some/path1")
  println("/some/path2")
  true

def findEmptyAndPrint(entry: cs214.Entry): Boolean =
  println("/some/path1")
  println("/some/path2")
  true

///////////////////////////////
// The following is optional //
///////////////////////////////

def findFirstByNameAndPrint(entry: cs214.Entry, name: String): Boolean =
  println("/some/path1")
  println("/some/path2")
  true
