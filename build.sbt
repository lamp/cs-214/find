name := "find"
scalaVersion := "3.5.0"
libraryDependencies += "com.lihaoyi" %% "os-lib" % "0.10.4"
libraryDependencies += "org.scalameta" %% "munit" % "1.0.1" % Test
enablePlugins(PackPlugin)
scalacOptions ++= Seq("-deprecation", "-feature", "-language:fewerBraces", "-Xfatal-warnings")
